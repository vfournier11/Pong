package com.ntfournier;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;

public class Pong extends ApplicationAdapter {
    public static final int WIDTH = 1920;
    public static final int PADDLE_OFFSET = 250;

    public static final int HEIGHT = 1080;

    public static final int PADDLE_WIDTH = 30;
    public static final int PADDLE_HEIGHT = 100;

    public static final int BALL_SIZE = 32;
    private static final float BALL_SPEED = 900;

    private BitmapFont font;

    private OrthographicCamera camera;
    private SpriteBatch batch;

    private Texture paddleTexture;
    private Texture ballTexture;

    private Rectangle leftPaddle;
    private Rectangle rightPaddle;
    private Ball ball;

    private float currentComputerSpeed = 800;

    private int leftPlayerScore = 0;
    private int rightPlayerScore = 0;
    private String score = String.format(scoreFormat, leftPlayerScore, rightPlayerScore);
    private static final String scoreFormat = "%d     -     %d";

    @Override
    public void create() {
        camera = new OrthographicCamera();
        camera.setToOrtho(false, WIDTH, HEIGHT);

        font = new BitmapFont();
        font.getData().setScale(10);
        font.setColor(Color.WHITE);

        batch = new SpriteBatch();

        paddleTexture = new Texture("paddle.png");
        ballTexture = new Texture("ball.png");

        leftPaddle = new Rectangle(PADDLE_OFFSET, HEIGHT / 2, PADDLE_WIDTH, PADDLE_HEIGHT);
        rightPaddle = new Rectangle(WIDTH - PADDLE_OFFSET - PADDLE_WIDTH, HEIGHT / 2, PADDLE_WIDTH, PADDLE_HEIGHT);

        ball = new Ball(WIDTH / 2, HEIGHT / 2, BALL_SIZE, BALL_SIZE);
        ball.setDirection(1, 0);
        ball.setSpeed(BALL_SPEED);
    }

    @Override
    public void render() {
        Gdx.gl.glClearColor(1, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        camera.update();
        batch.setProjectionMatrix(camera.combined);

        batch.begin();
        font.draw(batch, score, WIDTH / 2 - 300, HEIGHT - HEIGHT * .05f);

        batch.draw(paddleTexture, leftPaddle.x, leftPaddle.y);
        batch.draw(paddleTexture, rightPaddle.x, rightPaddle.y);
        batch.draw(ballTexture, ball.x, ball.y);
        batch.end();

        if (Gdx.input.isTouched()) {
            Vector3 touchPosition = new Vector3();
            touchPosition.set(Gdx.input.getX(), Gdx.input.getY(), 0);
            camera.unproject(touchPosition);
            leftPaddle.y = touchPosition.y - PADDLE_HEIGHT / 2;
        }

        if (Gdx.input.isKeyPressed(Input.Keys.UP)) {
            leftPaddle.y += 200 * Gdx.graphics.getDeltaTime();
        }

        if (Gdx.input.isKeyPressed(Input.Keys.DOWN)) {
            leftPaddle.y -= 200 * Gdx.graphics.getDeltaTime();
        }

        if (leftPaddle.y < 0) leftPaddle.y = 0;
        if (leftPaddle.y > HEIGHT - PADDLE_HEIGHT) leftPaddle.y = HEIGHT - PADDLE_HEIGHT;

        ball.update(Gdx.graphics.getDeltaTime());

        this.checkCollision();

        this.checkBallOut();

        this.playComputer(Gdx.graphics.getDeltaTime());
    }

    private void playComputer(float delta) {
        int direction = this.ball.y + BALL_SIZE / 2 < this.rightPaddle.y + PADDLE_HEIGHT / 2 ? -1 : 1;
        this.rightPaddle.y += this.currentComputerSpeed * direction * delta;
    }

    public void checkBallOut() {
        if (this.ball.x < 0 - BALL_SIZE) {
            ++this.rightPlayerScore;
            this.resetPlay();
            this.score = String.format(scoreFormat, leftPlayerScore, rightPlayerScore);
        }

        if (this.ball.x > WIDTH) {
            ++this.leftPlayerScore;
            this.currentComputerSpeed *= 1.1f;
            this.resetPlay();
            this.score = String.format(scoreFormat, leftPlayerScore, rightPlayerScore);
        }
    }

    private void resetPlay() {
        ball.setPosition(WIDTH / 2, HEIGHT / 2);
        ball.setDirection((float) Math.random(), (float) Math.random());
        ball.setSpeed(BALL_SPEED);
    }

    public void checkCollision() {
        if (leftPaddle.overlaps(ball)) {
            ball.setDirection(Math.abs(ball.direction.x), (float) Math.random());
            ball.setSpeed(ball.speed * 1.1f);
        }

        if (rightPaddle.overlaps(ball)) {
            ball.setDirection(-Math.abs(ball.direction.x), (float) Math.random());
            ball.setSpeed(ball.speed * 1.1f);
        }

        if (ball.y > HEIGHT - BALL_SIZE) ball.setDirection(ball.direction.x, -Math.abs(ball.direction.y));
        if (ball.y < 0) ball.setDirection(ball.direction.x, Math.abs(ball.direction.y));
    }

    @Override
    public void dispose() {
        batch.dispose();
        paddleTexture.dispose();
        ballTexture.dispose();
    }
}
