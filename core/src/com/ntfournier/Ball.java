package com.ntfournier;


import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class Ball extends Rectangle {
    Vector2 direction = new Vector2();
    float speed;

    public Ball(float x, float y, float width, float height) {
        super(x, y, width, height);
    }

    public void update(float delta) {
        this.x += this.direction.x * this.speed * delta;
        this.y += this.direction.y * this.speed * delta;
    }

    public void setDirection(float x, float y) {
        this.direction.x = x;
        this.direction.y = y;
        this.direction.nor();
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }
}
