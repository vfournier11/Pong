package com.ntfournier.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.ntfournier.Pong;

public class DesktopLauncher {
    public static void main(String[] arg) {
        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.title = "PONG";
        config.width = 1920;
        config.height = 1080;
        new LwjglApplication(new Pong(), config);
    }
}
